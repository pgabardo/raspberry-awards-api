Projeto
-----------
API RESTful para possibilitar a leitura da lista de indicados e vencedores
da categoria Pior Filme do Golden Raspberry Awards.


Requisitos técnicos
-------------------
Para executar o projeto, será necessário instalar os seguintes programas:

	JDK 11: Necessário para executar o projeto Java

	Maven 3.6.x: Necessário para realizar o build do projeto Java e rodar os testes

	Eclipse: Para desenvolvimento do projeto


Desenvolvimento
---------------

Para iniciar  é necessário clonar o projeto do GITLAB https://gitlab.com/pgabardo/raspberry-awards-api.git:

	cd "diretorio de sua preferencia"
	git clone https://gitlab.com/pgabardo/raspberry-awards-api.git
	
	Eclipse - file >> import >> importar projeto maven
	Build - botão direito no projeto>> maven >> update project
	Run - botão direito no projeto >> run as >> java application >> selecionar a classe RaspberryAwardsWsApplication e rodar

URL da Aplicação
---------------------
Para acessar a aplicação seguem os links abaixo:

URL LocalHost
	
	http://localhost:8080/raspberry-awards-api


Endpoint para buscar produtores com maior e menor intervalo de prêmios
--------------------
   
	http://localhost:8080/raspberry-awards-api/producer/interval-winners
    
Rodar teste de integração
--------------------
Executar comando maven na raiz do projeto
	
	mvn test  


