package br.com.texo.ws;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import br.com.texo.bean.IntervalVO;
import br.com.texo.bean.ResponseIntervalProducerVO;
import br.com.texo.listener.InsertDbComponent;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ProducerControllerTest {
    
    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    public void getProducersIntervalWinners() throws Exception {

        ResponseEntity<ResponseIntervalProducerVO> response = this.testRestTemplate
            .exchange("/producer/interval-winners", HttpMethod.GET, null, ResponseIntervalProducerVO.class);
        
        ResponseIntervalProducerVO respIntervalProducerVo = response.getBody();
        
        IntervalVO min = respIntervalProducerVo.getMin().get(0);
        IntervalVO max = respIntervalProducerVo.getMax().get(0);
        
        boolean minOk = findCsvFile(min.getProducer(), min.getPreviousWin());
        boolean maxOk = findCsvFile(max.getProducer(), max.getPreviousWin());
    
        assertEquals(true, minOk && maxOk);
    }
    
    private boolean findCsvFile(String producer, Integer release) throws Exception {
    	
    	boolean founded = false;
    	Reader reader = null;
        
        InputStream inputStream = InsertDbComponent.class.getClassLoader().getResourceAsStream("csv/movielist.csv");
        reader = new InputStreamReader(inputStream);
  	  
        try {
      	  
      	    // create csvparser with ';'
      	    CSVParser parser = new CSVParserBuilder().withSeparator(';').build();
            
      	    // create csvReader object and skip first Line
            CSVReader csvReader = new CSVReaderBuilder(reader)
                                      .withSkipLines(1).withCSVParser(parser)
                                      .build();
            
            List<String[]> allData = csvReader.readAll();
     
            // print Data
            for (String[] row : allData) {
                Long releaseCsv = Long.parseLong(row[0]);
                String producers = row[3];
                if(producers.contains(producer) && releaseCsv.intValue() == release.intValue()) {
                	founded = true;
                }
            }
            
            return founded;
            
        }catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    
}