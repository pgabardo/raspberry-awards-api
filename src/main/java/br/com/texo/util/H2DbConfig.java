package br.com.texo.util;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
public class H2DbConfig {
		
	@Value("${spring.datasource.driver-class-name}")
	private String dbDriverClass;
	@Value("${spring.datasource.username}")
	private String dbUsername;
	@Value("${spring.datasource.password}")
	private String dbPassword;
	@Value("${spring.datasource.url}")
	private String dbUrl;

    @Bean(name = "h2DataSource", autowireCandidate = true)
    public DataSource h2DataSource() throws NamingException {
    		
        try {
        	HikariDataSource dataSource = new HikariDataSource();
    		dataSource.setDriverClassName(dbDriverClass);
    		dataSource.setUsername(dbUsername);
    		dataSource.setPassword(dbPassword);
    		dataSource.setJdbcUrl(dbUrl);
    		return dataSource;
    		
		} catch (Exception e) {			
			e.printStackTrace();
			throw e;
		}
        
    }
}    

