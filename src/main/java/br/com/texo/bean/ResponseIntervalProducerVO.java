package br.com.texo.bean;

import java.util.List;

public class ResponseIntervalProducerVO {

	private List<IntervalVO> min;
	private List<IntervalVO> max;
	
	public List<IntervalVO> getMin() {
		return min;
	}
	public void setMin(List<IntervalVO> min) {
		this.min = min;
	}
	public List<IntervalVO> getMax() {
		return max;
	}
	public void setMax(List<IntervalVO> max) {
		this.max = max;
	}
}
