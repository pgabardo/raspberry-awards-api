package br.com.texo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class RaspberryAwardsWsApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(RaspberryAwardsWsApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(RaspberryAwardsWsApplication.class);
	}
	
}
