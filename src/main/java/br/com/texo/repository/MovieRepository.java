package br.com.texo.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import br.com.texo.bean.entity.MovieEntity;

@Service
public interface MovieRepository extends CrudRepository<MovieEntity, Integer> {

    List<MovieEntity> findByTitle(String title);
    
    List<MovieEntity> findByWinnerOrderByReleaseAsc(String winner);

}
