package br.com.texo.ws.service;

import br.com.texo.bean.ResponseIntervalProducerVO;

public interface ProducerService {

	ResponseIntervalProducerVO getProducersIntervalWinners() throws Exception;

}
