package br.com.texo.ws.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.texo.bean.IntervalVO;
import br.com.texo.bean.ResponseIntervalProducerVO;
import br.com.texo.bean.entity.MovieEntity;
import br.com.texo.repository.MovieRepository;

@Service
public class ProducerServiceImpl implements ProducerService {
    protected final Log LOG = LogFactory.getLog(ProducerServiceImpl.class);
    
    @Autowired
    private MovieRepository movieRepository;

	@Override
	public ResponseIntervalProducerVO getProducersIntervalWinners() throws Exception {
		List<MovieEntity> listMovies = movieRepository.findByWinnerOrderByReleaseAsc("yes");	
		
		Map<String, List<MovieEntity>> hashMap = new HashMap<String,List<MovieEntity>>();
		
		for (MovieEntity movieEntity : listMovies) {
			String[] produtores = movieEntity.getProducers().replace(" and ", ",").split(",");
			for (int i = 0; i < produtores.length; i++) {
				String prod = produtores[i].trim();
				if(prod.equals("")) {
					continue;
				}
				List<MovieEntity> premiosDoProdutor = listMovies.stream().filter(m-> m.getProducers().contains(prod)).collect(Collectors.toList());
				hashMap.put(prod, premiosDoProdutor);
			}
		}
		
		List<IntervalVO> listInterval = new ArrayList<IntervalVO>();
		
		//create interval list
		for (Map.Entry<String, List<MovieEntity>> set : hashMap.entrySet()) {

           if(set.getValue().size() > 1) {
        	   for (int i = 0; i < set.getValue().size() -1; i++) {
        		   MovieEntity movPrev = set.getValue().get(i);
        		   MovieEntity movFollow = set.getValue().get(i+1);
        		   IntervalVO inter = new IntervalVO();
            	   inter.setProducer(set.getKey());
            	   inter.setPreviousWin(movPrev.getRelease().intValue());
            	   inter.setFollowingWin(movFollow.getRelease().intValue());
            	   inter.setInterval(movFollow.getRelease().intValue() - movPrev.getRelease().intValue());
            	   
            	   listInterval.add(inter);
        	   }
           }
        }
		
		Integer minInterval = null;
		Integer maxInterval = null;
		
		for (IntervalVO intervalVO : listInterval) {
			if(minInterval == null || minInterval.intValue() > intervalVO.getInterval().intValue()) {
				minInterval = intervalVO.getInterval();
			}
			
			if(maxInterval == null || maxInterval.intValue() < intervalVO.getInterval().intValue()) {
				maxInterval = intervalVO.getInterval();
			}
		}
		
		final int minIntervalFinal = minInterval.intValue();
		final int maxIntervalFinal = maxInterval.intValue();
		
		List<IntervalVO> listMinInterval = listInterval.stream().filter(m-> m.getInterval().intValue() == minIntervalFinal).collect(Collectors.toList());
		List<IntervalVO> listMaxInterval = listInterval.stream().filter(m-> m.getInterval().intValue() == maxIntervalFinal).collect(Collectors.toList());
		
		ResponseIntervalProducerVO resp = new ResponseIntervalProducerVO();
		resp.setMin(listMinInterval);
		resp.setMax(listMaxInterval);
		
		return resp;
	}

    
}
