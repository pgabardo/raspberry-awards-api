package br.com.texo.ws;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.texo.bean.ResponseIntervalProducerVO;
import br.com.texo.ws.service.ProducerService;


@RestController
@RequestMapping("/producer")
public class ProducerController
{

	private Logger logger = Logger.getLogger(ProducerController.class.getName());
	
	@Autowired
	private ProducerService producerService;

	
	@RequestMapping(
			path = "/interval-winners", 
			produces=MediaType.APPLICATION_JSON_VALUE, 
			method = RequestMethod.GET)
    public ResponseEntity<?>  getProducersIntervalWinners ()
	{
		logger.info("/producer/interval-winners");
		
		try {
			ResponseIntervalProducerVO responseIntervalProducerVO = producerService.getProducersIntervalWinners();
			
			if(responseIntervalProducerVO == null 
					|| responseIntervalProducerVO.getMin() == null 
					|| responseIntervalProducerVO.getMin().size() == 0) {
				
				//not found
				return ResponseEntity.noContent().build();
			}
			
			
			return ResponseEntity.ok(responseIntervalProducerVO);	
			
		} catch (Exception e) {		
			e.printStackTrace();
			return ResponseEntity.internalServerError().body(e.getMessage());
		}
		
	}
	
}
