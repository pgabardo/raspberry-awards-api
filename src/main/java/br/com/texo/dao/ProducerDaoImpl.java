package br.com.texo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.texo.bean.entity.MovieEntity;

@Service
public class ProducerDaoImpl implements ProducerDao{
	
	@Autowired
	private DataSource h2DataSource;
	
	private Logger logger = Logger.getLogger(ProducerDaoImpl.class.getName());
	
	@Override
	public List<MovieEntity> getProducersIntervalWinners() throws Exception {
		
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try{
			  con = h2DataSource.getConnection();
					
			  pst = con.prepareStatement(" select m.producers, "
			  		+ " m.release "
			  		+ " from movie m "
			  		+ " where m.winner = 'yes' ");
				
				
				rs = pst.executeQuery();
				
				List<MovieEntity> listMovies = new ArrayList<MovieEntity>();
				
				while (rs.next()){
					MovieEntity movie = new MovieEntity();
					movie.setProducers(rs.getString("producers"));
					movie.setRelease(rs.getLong("release"));
					
					listMovies.add(movie);					
				}
				
				return listMovies;				
				
			} catch (SQLException e) {
				logger.log(Level.SEVERE, "Erro ao buscar lista de filmes");
				e.printStackTrace();
				throw e;
				
			}finally {
				if(rs != null) {
					rs.close();
				}
				if(pst != null) {
					pst.close();
				}
				if(con != null) {
					con.close();
				}			
			}
		   		
	}
	

}
