package br.com.texo.dao;

import java.util.List;

import br.com.texo.bean.entity.MovieEntity;

public interface ProducerDao{
	
	public List<MovieEntity> getProducersIntervalWinners() throws Exception;
}
