package br.com.texo.listener;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import br.com.texo.bean.entity.MovieEntity;
import br.com.texo.repository.MovieRepository;

@Component
public class InsertDbComponent {

  private final Logger LOG = Logger.getLogger(InsertDbComponent.class.getName());

  @Autowired
  private MovieRepository movieRepository;
  
  @EventListener(ApplicationReadyEvent.class)
  public void insertMovieList() throws Exception {
	
      LOG.info("*********************************************************");
      LOG.info("**********START - insert movie list**********************");
      LOG.info("*********************************************************");
      
      Reader reader = null;
     
      InputStream inputStream = InsertDbComponent.class.getClassLoader().getResourceAsStream("csv/movielist.csv");
      reader = new InputStreamReader(inputStream);
	  
      try {
    	  
    	  // create csvparser with ';'
    	  CSVParser parser = new CSVParserBuilder().withSeparator(';').build();
          
    	  // create csvReader object and skip first Line
          CSVReader csvReader = new CSVReaderBuilder(reader)
                                    .withSkipLines(1).withCSVParser(parser)
                                    .build();
          
          List<String[]> allData = csvReader.readAll();
   
          // print Data
          for (String[] row : allData) {
        	  MovieEntity movie = new MovieEntity();
              
              movie.setRelease(Long.parseLong(row[0]));
              movie.setTitle(row[1]);
              movie.setStudios(row[2]);
              movie.setProducers(row[3]);
              movie.setWinner(row[4]);
              
              LOG.info("saving movie: " + movie.getRelease() + ", " + movie.getTitle() + ", " + movie.getStudios() + ", " + movie.getProducers() + ", " +movie.getWinner());
              //save movie objetc
              movieRepository.save(movie);
          }
      }
      catch (Exception e) {
          e.printStackTrace();
          throw e;
      }
      
      LOG.info("*********************************************************");
      LOG.info("**********END -insert movie list*************************");
      LOG.info("*********************************************************");
  }
  
}